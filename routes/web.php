<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('tes', function(){
	dd(App\User::find(14)->orphanage);
});

Route::get('command', function() {
    return view('artisan-command');
});

Route::post('execute-command', function(Illuminate\Http\Request $request) {
    $input = $request->all();
    $value = Artisan::call($input['command']);
    $request->session()->flash('output', $value);
    return redirect()->back();
});

Route::get('/checkout', function () {
    return view('checkout');
});

Route::get('/', function () {
    $products = App\Product::paginate(6);
    return view('home', compact('products'));
});
Route::get('home', function () {
    return redirect('/');
});
Route::get('/dashboard', 'HomeController@index');
Route::get('/contact-us', function(){
    return view('contact-us');
});
Route::resource('products', 'ProductController');

Auth::routes();
Route::group(['middleware' => 'auth'], function() {
    Route::resource('orphanages', 'OrphanageController');
    Route::resource('users', 'UserController');
    Route::resource('carts', 'CartController');
	Route::resource('header-transactions', 'HeaderTransactionController');
	Route::resource('detail-transactions', 'DetailTransactionController');
});
