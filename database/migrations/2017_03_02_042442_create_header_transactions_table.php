<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeaderTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('header_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id'); //pembeli
            $table->text('shipping_address');
            $table->string('recipients_name');
            $table->string('recipients_contact');

            //['1. not transfered', '2. waiting the delivery', '3. done', '4. cancel - not receive cashback yet', '5. cancel - done']
            $table->string('approval_status'); 

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('header_transactions');
    }
}
