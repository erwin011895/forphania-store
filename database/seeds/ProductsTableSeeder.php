<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::truncate();

        $product = new Product;
        $product->orphanage_id = 1;
        $product->name = "Wayang Kulit";
        $product->price = 15000;
        $product->stock = 10;
        $product->description = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
        $product->image_url = 'public/storage/wayang.jpg';
        $product->save();

        $product = new Product;
        $product->orphanage_id = 1;
        $product->name = "Wayang Kulit 2";
        $product->price = 15000;
        $product->stock = 10;
        $product->description = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
        $product->image_url = 'public/storage/wayang2.jpg';
        $product->save();

        for ($i=0; $i < 9; $i++) { 
            $product = new Product;
            $product->orphanage_id = 1;
            $product->name = "Wayang Kulit 10";
            $product->price = 15000;
            $product->stock = 10;
            $product->description = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
            $product->image_url = 'public/storage/wayang2.jpg';
            $product->save();
        }
    }
}
