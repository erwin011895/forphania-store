<?php

namespace App\Http\Controllers;

use App\Orphanage;
use Illuminate\Http\Request;

class OrphanageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('orphanages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required'
        ]);

        $orphanage = new Orphanage;
        $orphanage->name = $request->input('name');
        $orphanage->address = $request->input('address');
        $orphanage->partner_id = auth()->id();
        $orphanage->save();

        $request->session()->flash('message', "success");
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Orphanage  $orphanage
     * @return \Illuminate\Http\Response
     */
    public function show(Orphanage $orphanage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Orphanage  $orphanage
     * @return \Illuminate\Http\Response
     */
    public function edit(Orphanage $orphanage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Orphanage  $orphanage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Orphanage $orphanage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Orphanage  $orphanage
     * @return \Illuminate\Http\Response
     */
    public function destroy(Orphanage $orphanage)
    {
        //
    }
}
