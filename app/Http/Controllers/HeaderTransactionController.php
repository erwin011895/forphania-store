<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HeaderTransaction;
use App\DetailTransaction;
use App\Cart;

class HeaderTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'recipients_name'    => 'required',
            'recipients_contact' => 'required',
            'shipping_address'   => 'required',
            'qty.*' => 'required',
            'notes.*' => 'required',
        ]);

        $user = auth()->user();
        $cart = $user->cart;

        $ht = new HeaderTransaction;
        $ht->user_id = $user->id;
        $ht->shipping_address   = $request->input('shipping_address');
        $ht->recipients_name    = $request->input('recipients_name');
        $ht->recipients_contact = $request->input('recipients_contact');
        $ht->approval_status = "1. not transfered";
        $ht->save();

        $i=0;
        foreach ($cart->products as $product) {
            $dt = new DetailTransaction;
            $dt->product_id = $product->id;
            $dt->product_price = $product->price;
            $dt->qty = $request->input('qty')[$i];
            $dt->notes = $request->input('notes')[$i];
            $ht->detail_transactions()->save($dt);

            $i++;
        }
        
        $request->session()->flash('message', 'checkout success');

        return redirect('/');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
