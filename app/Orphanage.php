<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orphanage extends Model
{
	use SoftDeletes;
    protected $connection = 'forphania-client';

    public function user()
    {
        return $this->belongsTo('App\User', 'partner_id');
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }
}
