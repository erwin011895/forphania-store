<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function orphanage()
    {
        return $this->belongsTo('App\Orphanage');
    }

    public function carts()
    {
        return $this->belongsToMany('App\Cart');
    }

    public function detail_transactions()
    {
        return $this->hasMany('App\DetailTransaction');
    }

    public function reviews()
    {
        return $this->hasMany('App\Review');
    }
}
