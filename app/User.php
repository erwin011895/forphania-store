<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $connection = 'forphania-client';

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'address', 'phone1', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function orphanage()
    {
        return $this->hasOne('App\Orphanage', 'partner_id');
    }

    public function cart()
    {
        return $this->hasOne('App\Cart');
    }

    public function header_transactions()
    {
        return $this->hasMany('App\HeaderTransaction');
    }

    public function reviews()
    {
        return $this->hasMany('App\Review');
    }
}
