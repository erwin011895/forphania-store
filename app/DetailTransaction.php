<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailTransaction extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function header_transaction()
    {
        return $this->belongsTo('App\HeaderTransaction');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
