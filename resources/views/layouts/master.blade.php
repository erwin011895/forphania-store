<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Forphania - @yield('title')</title>
	<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
		<script src="js/mq.js"></script>
	<![endif]-->
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<meta charset="UTF-8">
	<meta name="viewport" content="initial-scale=1, maximum-scale=1">
	<meta name="viewport" content="width=device-width">

	<meta csrf_token="{{csrf_token()}}">
	<!-- Css Files Start -->
	<link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css" /><!-- All css -->
	<link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css" /><!-- Bootstrap Css -->
	<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="css/customIE.css" /><![endif]-->
	<link href="{{asset('css/font-awesome.css')}}" rel="stylesheet" type="text/css" /><!-- Font Awesome Css -->
	<link href="{{asset('css/font-awesome-ie7.css')}}" rel="stylesheet" type="text/css" /><!-- Font Awesome iE7 Css -->
	<!-- Css Files End -->
	<!-- Social Icons no JS -->
	<noscript>
		<link rel="stylesheet" type="text/css" href="{{asset('css/nj.css')}}" />
	</noscript>
	<!-- Social Icons no JS end-->
	
	<link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css" /><!-- All css -->
	<style>
		
	</style>
</head>
<body>
<!-- Start Main Wrapper -->
<div class="wrapper">
<!-- Start of Header -->
	<header>
  <!-- Start Main Header -->
	<section style="padding: 10px;">
	  <section class="container-fluid container">
			<section class="row-fluid">
				<section class="span2">
					<a href="http://forphania.com">
						<img src="{{asset('images/logo.png')}}" style="background-color: #4CAF50; width: 80px; height: 80px; border-radius: 100%;">
					</a>
				</section>
			</section>
		</section>
  </section>

	<!-- Main Nav Bar -->
		<nav id="nav">
			<section class="container-fluid container">
				<section class="row-fluid">
		  <div class="navbar navbar-inverse">
			<div class="navbar-inner">
			  <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
			  <div class="nav-collapse collapse">
				<ul class="nav">
					<li><a href="{{url('/')}}"> Beranda </a></li>
				  <!-- <li class="dropdown">
				  	<a class="dropdown-toggle" href="about-us.html">Tentang Kita<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="team.html"> Organizers</a></li>
							<li><a href="career.html">Careers</a></li>
						</ul>
				  </li> -->
					<li><a href="{{url('contact-us')}}"> Kontak </a></li>
					@if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('/logout') }}"
                                        onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
				</ul>
			  </div>
			  <!--/.nav-collapse -->
			</div>
			<!-- /.navbar-inner -->
		  </div>
		  <!-- <div class="nav_search pull-right"> <form method="post" actoion="#"> <input type="text" value="Search Here..." name="s" id="s" /> <button><i class="icon-search"></i> </button></form></div> -->
		  <!-- /.navbar -->
				</section>
			</section>
		</nav>
		<!-- End Main Nav Bar -->
	</header>
<!-- End of Header -->

<!-- Title & BreadCrumbs -->
<section class="container-fluid container">
	<section class="row-fluid">
			<section id="donation_box">
				<section class="container container-fluid">
					<section class="row-fluid">
						<div class="span7 first"> <h2> Our Store </h2> </div>
						<div class="span5 title_right">
			 
							<div class="dropdown" id="cart_dropdown">
								@if (Auth::guest())
									<h4> Login terlebih dahulu untuk melihat keranjang belanja. </h4>
								@else
									<span> You have <span class="count">{{auth()->user()->cart->products->count()}} item </span> in your cart. </span>
									
									<a data-toggle="dropdown" class="dropdown-toggle" role="button" id="cart_down">
										<i class="icon-shopping-cart"></i>
										Cart
										<span class="caret"></span>
									</a>
									<div class="dropdown-menu" aria-labelledby="cart_down" role="menu" id="cart_down_content">
										<ul id="cart">
											@forelse(auth()->user()->cart->products as $product)
												<li> 
													<div class="product_name_custom span7"> {{$product->name}}</div>
													<div class="product_name_custom span2"> {{$product->price}}</div>
													<div class="actions">
														<a onclick="event.preventDefault();document.getElementById('cart-delete-form-{{$product->id}}').submit();"> <i class="icon-trash"></i></a>
														
														<form id="cart-delete-form-{{$product->id}}" action="{{route('carts.destroy', auth()->user()->cart)}}" method="POST">
															{{csrf_field()}}
															{{method_field('DELETE')}}
															<input type="hidden" name="product_id" value="{{$product->id}}">
														</form>
													</div>
												</li>
											@empty
												<li>
													<div class="product_name_custom span7"> You haven't buy anything..</div>
												</li>
											@endforelse
										</ul>
										<div class="cart_total_checkout"> 
											<!-- <span class="pull-left col1"> Total Rp </span> 
											<span class="pull-left col1"> 0 </span>  -->
											<a href="{{route('carts.index')}}" class="pull-right continue_shopping"> View Cart </a>
										</div>
									</div>
								@endif
							</div>
						</div>
					</section>
				</section>
		</section>		<!-- end of Page Title -->
	</section>
	<section class="row-fluid">
		<!-- BreadCrumbs -->
			<figure id="breadcrumbs" class="span12">
				<ul class="breadcrumb">
				<li><a href="http://forphania.com/">Forphania</a> <span class="divider">/</span></li>
				<li class="active">Store</li>
				</ul>
			</figure>
		<!-- End of breadcrumbs -->
	</section>
</section>
<!-- End of Tile & Breadcrumbs -->
  
@yield('content')
	 
<!-- Start of Footer -->
	<footer id="footer" class="">
	
	<!-- Start of Footer 1 -->
		<section class="footer_1">
		<section class="container-fluid container">
			<section class="row-fluid">
				<section class="span12 first" id="footer_main">
					
					<section class="span3 first widget">
						<!-- <h4>Subscribe Now</h4>
						<form id="contact_form" method="post" action="">
							<label> Your Email</label>
							<input type="email" value="" placeholder="forphania@example.com" name="email" required />
							<input type="submit" name="submit" value="Subscribe" />
						</form> -->
						
					</section>
				</section>
			</section>
		</section>
	</section>
	<!-- End of Footer 1 -->
	
	<!-- Start of Footer 2 -->
	<section class="top_bar" style="color: #FFFFFF">
	<section class="container-fluid container">
		<section class="row-fluid">
			<article class="span6">
				<ul class="details">
					<li><i class="icon-map-marker"> </i> Jalan Raya Kemanggisan, Jakarta, Indonesia</li>
					<li><i class="icon-mobile-phone"> </i> +62822 1048 0625</li>
					<li><i class="icon-envelope-alt"> </i> store@forphania.com</li>
				</ul>
			</article>
			<article class="span4 offset2"> 		
				<ul class="social">
					<li> <a href="#" class="s1"> Facebook</a> </li>
					<li> <a href="#" class="s5"> Twitter</a> </li>

				</ul>
			</article>
		</section>
		<section class="row-fluid">
			<article class="span6">
				<ul class="footer_nav">
					<li> <a href="index.html">Home</a> </li>
					<li> <a href="http://forphania.com/" target="_blank">About Us</a> </li>
					<li> <a href="http://forphania.com/" target="_blank">Policy</a> </li>
					<li> <a href="http://forphania.com/" target="_blank">Support</a> </li>
				</ul>
				<p> Copyright © 2017 - Forphania <a href="http://forphania.com"> forphania.com </a></p>
				
			</article>
		</section>
	</section>
  </section>
	<!-- End of Footer 2 -->
	</footer>
<!-- End of Footer -->
</div>
<!-- End Main Wrapper -->
<!-- JS Files Start -->
<script type="text/javascript" src="{{asset('js/lib-1-9-1.js')}}"></script><!-- lib Js -->
<script type="text/javascript" src="{{asset('js/lib-1-7-1.js')}}"></script><!-- lib Js -->
<script type="text/javascript" src="{{asset('js/modernizr.js')}}"></script><!-- Modernizr -->
<script type="text/javascript" src="{{asset('js/easing.js')}}"></script><!-- Easing js -->
<script type="text/javascript" src="{{asset('js/bootstrap.js')}}"></script><!-- Bootstrap -->
<script type="text/javascript" src="{{asset('js/bxslider.js')}}"></script><!-- BX Slider -->
<script type="text/javascript" src="{{asset('js/clearInput.js')}}"></script><!-- Input Clear -->
<script type="text/javascript" src="{{asset('js/smooth-scroll.js')}}"></script><!-- smooth Scroll -->
<script type="text/javascript" src="{{asset('js/prettyPhoto.js')}}"></script><!-- Pretty Photo -->
<script type="text/javascript" src="{{asset('js/social.js')}}"></script><!-- Social Media Hover Effect -->
<script type="text/javascript" src="{{asset('js/countdown.js')}}"></script><!-- Event Counter -->
<script type="text/javascript" src="{{asset('js/custom.js')}}"></script><!-- Custom / Functions -->
<script type="text/javascript" src="{{asset('js/cycle.js')}}"></script><!-- Cycle -->
<script type="text/javascript" src="{{asset('js/checkout.js')}}"></script><!-- Checkout -->
<!--[if IE 8]>
	 <script src="{{asset('js/ie8_fix_maxwidth.js')}}" type="text/javascript"></script>
<![endif]--> 
<script type="text/javascript">
  /* <![CDATA[ */
			  presentationCycle.init();
/* ]]> */
</script>
@stack('scripts')
</body>
</html>
