@extends('layouts.master')

@section('content')
<section id="content" class="mbtm">
    <form method="POST" action="{{ url('/register') }}">
        {{ csrf_field() }}
        <section class="container container-fluid">
            <div class="span12">
                <label>Name</label>
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                @if ($errors->has('name'))
                    <span class="alert alert-error">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="span12">
                <label>Email</label>
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="example@mail.com" autofocus>
                @if ($errors->has('email'))
                    <span class="alert alert-error">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="span12">
                <label>Password</label>
                <input id="password" type="password" class="form-control" name="password" required placeholder="secret password">
                @if ($errors->has('password'))
                    <span class="alert alert-error">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="span12">
                <label>Password Confirmation</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
            </div>
            <div class="span12">
                <label>Address</label>
                <textarea class="form-control" name="address" required></textarea>
                @if ($errors->has('password'))
                    <span class="alert alert-error">
                        <strong>{{ $errors->first('address') }}</strong>
                    </span>
                @endif
            </div>
            
            <div class="span12">
                <label>Phone</label>
                <input id="phone" type="text" class="form-control" name="phone1" required placeholder="08xxxxxxxxxx">
                @if ($errors->has('phone1'))
                    <span class="alert alert-error">
                        <strong>{{ $errors->first('phone1') }}</strong>
                    </span>
                @endif
            </div>
            <div class="span12">
                <button type="submit" class="btn btn-primary">
                    Login
                </button>

                <!-- <a class="btn btn-link" href="{{ url('/password/reset') }}">
                    Forgot Your Password?
                </a> -->
            </div>
        </section>
    </form>
</section>
@endsection
