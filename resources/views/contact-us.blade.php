@extends('layouts.master')

@section('title', 'Detail')

@section('content')

<!-- Page Content Container -->
<section id="content" class="mbtm product_detal">
	<section class="container-fluid container">

		<section class="row-fluid">
			@if(Session::has('message'))
				<div class="alert alert-success">
	                <button type="button" class="close" data-dismiss="alert">×</button>
	                <p>{{session('message')}}</p>
                </div>
			@endif
		</section>

		<section class="row-fluid">
			<!-- <section id="contact_form" class="span6 form">
				<h3> Contact Form </h3>
				<p> Thank you for visiting our website. Please fill out the following form to request information about our products or to provide feedback about our site. When you are finished, click the ‘Submit’ button to send us your message. </p>
				
				<form method="post" action="contact.php">
					<input type="text" value="Enter Your Name" name="name" required/>
					<input type="email" value="Email Address" name="email" required />
					<textarea name="comments" cols="10" rows="10" required> Enter your Comment</textarea>
					<input type="submit" class="btns" value="Submit" name="submit" />
				</form>
			</section> -->
		
			<section id="contact_info" class="span6 contact_info">
				<h3> Contact Information </h3>

				<figure class="span12 first">
					<p>Thank you for visiting our website. If you have any question, you can contact us through the email.</p>
				</figure>
				
				<figure class="span12 first"> <i class="icon-mobile-phone"></i> admin@forphania.com</figure>
				
				<figure class="span12 first"> <i class="icon-envelope-alt"></i> 082210480625 </figure>
					
					<figure class="span12 first"> 
					<div class="span12 fisrt">
					<i class="icon-map-marker"></i> 
					Jalan Anggrek Cakra, Kemanggisan, Jakarta Barat<br />
	 				</div>
					</figure>
	 

					<!-- <figure id="n_social" class="span12 first"> 
					
						<a href="#"> <i class="icon-facebook"></i> </a> 
						<a href="#"> <i class="icon-twitter"></i> </a> 
						<a href="#"> <i class="icon-google-plus"></i> </a> 
						<a href="#"> <i class="icon-linkedin"></i> </a> 
						<a href="#"> <i class="icon-pinterest"></i> </a> 
						
					
					</figure> -->
					
			</section><!-- /.contact_info -->
		</section><!-- /.row-fluid -->
	</section><!-- /.container-fluid -->

</section>
<!-- Page Content Container -->
@endsection

@push('scripts')

@endpush