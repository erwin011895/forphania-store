@extends('layouts.master')

@section('content')
<section id="content" class="mbtm">
    @if(session()->has('message'))
        {{session('message')}}
    @endif

    <form action="{{route('orphanages.store')}}" method="POST">
        {{ csrf_field() }}
        <section class="container container-fluid">
            <div class="span12">
                <label>Orphanage Name</label>
                <input type="text" name="name" placeholder="Orphanage Name" required value="{{ old('name') }}">
                @if ($errors->has('name'))
                    <span class="alert alert-error">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="span12">
                <label>Address</label>
                <textarea name="address" placeholder="Address"></textarea>
                @if ($errors->has('address'))
                    <span class="alert alert-error">
                        <strong>{{ $errors->first('address') }}</strong>
                    </span>
                @endif
            </div>
            <div class="span12">
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
            </div>
        </section>
    </form>
</section>

@endsection
