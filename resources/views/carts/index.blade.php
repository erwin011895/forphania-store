@extends('layouts.master')

@section('title', 'Your Cart')

@section('content')
<!-- Page Content Container -->
<section id="content" class="mbtm cart">
	<section class="container-fluid container">
		<section class="row-fluid">
			<form id="comments_form" action="{{ route('header-transactions.store') }}" method="POST">
				{{csrf_field()}}
				<div class="cart_table_holder">
					<table width="100%" cellpadding="10" border="0">
		 				<thead>
							<tr>
								<th width="60%" colspan="2">Description</th>
								<th width="10%">Price</th>
								<th width="15%">Quantity</th>
								<th width="10%">Total</th>
								<th width="5%">&nbsp;</th>
							</tr>
						 </thead>
						 <tbody>
						 	@foreach($cart->products as $product)
								<tr>
									<td width="14%" class="img"> <img src="{{asset('public/storage/wayang.jpg')}}" alt="Cart Product" /></td>
									<td> 
										<div class="product_name">{{$product->name}}</div>
										<figure>
											<textarea placeholder="Notes on this item" name="notes[]"></textarea>
											@if($errors->has('notes.'.$loop->index))
												<p class="text-error">*Please fill the notes</p>
											@endif
										</figure>
									</td>
									<td class="price">Rp <span class="product-price">{{$product->price}}</span></td>
									<td> 
										<input type="number" value="1" class="item-qty" name="qty[]" required />
										@if($errors->has('qty.'.$loop->index))
											<span class="text-error">Please specify the quantity</span>
										@endif
									</td>
									<td class="price"> Rp <span class="subtotal">{{$product->price}}</span></td>
									<td> 
										<form id="remove-cart-item-{{$product->id}}" action="{{ route('carts.destroy', $cart) }}" method="POST" style="display: none;">
	                                        {{ csrf_field() }}
	                                        <input type="hidden" name="product_id" value="{{$product->id}}">
	                                    </form>

										<a class="cbtn" onclick="event.preventDefault();
											var inputMethod = document.createElement('input');
											inputMethod.setAttribute('type', 'hidden');
											inputMethod.setAttribute('name', '_method');
											inputMethod.setAttribute('value', 'DELETE');
											var form = document.getElementById('remove-cart-item-{{$product->id}}');
											form.appendChild(inputMethod)
											form.submit()"> 
											Remove 
										</a> 

									</td>

								</tr>
					
			 				  	<tr>
									<!-- <td colspan="4" width="85%">  Estimated Shipping: </td> -->
									<!-- <td colspan="2" width="15%" class="b_price"> <sup>Rp</sup>15.000 </td> -->
			 					</tr>
						 	@endforeach
		                </tbody>
						<tfoot>
								<tr>
									<td colspan="4" width="85%">  </td>
									<td colspan="2" width="15%" class="total_price"> 
										<div class="total"> TOTAL </div>
										Rp <span id="total-price"></span>
									</td>
								</tr>
						</tfoot>
					</table>

				</div>

				<hr>

			
                <figure class="span7">
                    <p>After checkout, you have to transfer yhe payment to account 527111111 (Nirwan)</p>
                    <p>You will be confirmed when item has been sent to the address you provide.</p>
                </figure>

                <figure class="span4 first">
                    <div class="input_wrapper">
                        <input type="text" name="recipients_name" placeholder="Recipients Name" required />
                        <label> <i class="icon-user"></i> </label>
                        @if($errors->has('recipients_name'))
	                        <span class="text-error">Please fill the Recipient Name</span>
                    	@endif
                    </div>

                    <div class="input_wrapper">
                        <label> <i class="icon-phone"></i> </label>
                        <input type="text" name="recipients_contact" placeholder="Recipients Contact" required />
                        @if($errors->has('recipients_contact'))
	                        <span class="text-error">Please fill the Recipient Contact</span>
	                    @endif
                    </div>

                    <figure class="">
                        <textarea rows="10" cols="30" placeholder="Shipping Address" name="shipping_address"></textarea>
                        @if($errors->has('shipping_address'))
	                        <span class="text-error">Please fill the shipping address</span>
                        @endif
                    </figure>

                    <input type="submit" value="Checkout" />

                </figure>

            </form>
 
		</section>
	</section>

</section>
<!-- Page Content Container -->
@endsection
@push('scripts')
<script>
	function calculateTotalPrice() {
		var subtotals = $('.subtotal').toArray();
		var total = 0;
		for (var i = 0; i < subtotals.length; i++) {
			total += parseInt($(subtotals[i]).text());
		}
		$('#total-price').text(total);
	}


	$(document).ready(function() {
		calculateTotalPrice();

		$('.item-qty').on('change paste keyup', function(event) {
			var qty = $(this).val();
			var price = $(this).parent().prev().find('.product-price').text();
			$(this).parent().next().find('.subtotal').text(qty * price);
			calculateTotalPrice();
		});
	});
</script>
@endpush