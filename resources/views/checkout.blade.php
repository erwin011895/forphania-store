@extends('layouts.master')

@section('title', 'Checkout')

@section('content')

<section id="content" class="mbtm checkout">
	<section class="container-fluid container">
        <h3>Checkout</h3>
	
		<section class="row-fluid">
            <form id="comments_form">
                <figure class="span4 first">
                    <div class="input_wrapper">
                        <input type="text" name="recipients_name" placeholder="Recipients Name" />
                        <label for="name"> <i class="icon-user"></i> </label>
                    </div>

                    <div class="input_wrapper">
                        <label for="name"> <i class="icon-phone"></i> </label>
                        <input type="text" name="recipients_contact" placeholder="Recipients Contact" />
                    </div>
                    <figure class="">
                        <textarea rows="10" cols="30" placeholder="Shipping Address"></textarea>
                    </figure>

                    <input type="submit" value="Checkout" />

                </figure>

                <figure class="span8">
                    <p>After checkout, you have to transfer yhe payment to account 527111111 (Nirwan)</p>
                    <p>You will be confirmed when item has been sent to the address you provide.</p>
                </figure>
            </form>
		</section>
	</section>

</section>
<!-- Page Content Container -->
	 

@endsection