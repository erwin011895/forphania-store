@extends('layouts.master')

@section('title', 'Detail')

@section('content')

<!-- Page Content Container -->
<section id="content" class="mbtm product_detal">
	<section class="container-fluid container">
	
		<section class="row-fluid">
			@if(Session::has('message'))
				<div class="alert alert-success">
	                <button type="button" class="close" data-dismiss="alert">×</button>
	                <p>{{session('message')}}</p>
                </div>
			@endif
		</section>

		<section class="row-fluid">
			<figure class="span5 first" id="product_image">
				<div class="pro_img" id="pro_img">
					<img src="{{asset('images/wayang-detail.jpg')}}" alt="Default image" /> 
				</div>
			</figure>
				
			<figure class="span7" id="product_info">
				<h3>{{$product->name}}</h3>
				
				<div class="price_holder"> 
					<span class="current_price"> Price: Rp {{$product->price}}</span>
				</div>
				
				<div class="description_holder"> 
					{{$product->description}}
				</div>
					
				<ul class="stock_status">
					<li> Stock: 10 </li>
				</ul>
					
				<figure id="n_social" class="product_detail_social"> 
					<span class="span6">
						<!-- {{$product->reviews->count()}} Reviews -->
					</span>

					<figure class="span6" id="proudct_options">
						<form action="{{route('carts.store')}}" method="POST">
							{{csrf_field()}}
							@if($errors->has('product_id'))
								{{$errors->get('product_id')}}
							@endif
							<input type="hidden" name="product_id" value="{{$product->id}}">				
							<button> Add to <i class="icon-shopping-cart"></i> </button>
						</form>
					</figure>
				</figure>

			</figure>

			<figure class="span12 first" id="product_information">
				<div class="span4 first">
					<h3> Orphanage Information </h3>
					<div class="" id="accordion2">
						<ul class="product_properties">
							<li> <i class="icon-chevron-right"></i> Orphanage Name: {{$product->orphanage->name}}</li>
							<li> <i class="icon-chevron-right"></i> Orphanage Address: {{$product->orphanage->address}}</li>
						</ul>
					</div>
				</div>
				
				<!-- <div class="span8">
					<ul id="comments">
						@forelse($product->reviews as $review)
							<li> 
								<div class="span12 inner"> 
									<h3>{{$review->user->name}}</h3>
									<p>{{$review->content}}</p>
									<p> {{$review->created_at}}</p>
								</div>
							</li>
						@empty
							<li> 
								<div class="span12 inner"> 
									This Product does not have any review yet, Be first to give the review.
								</div>
							</li
						@endforelse
						<li>
							<div class="inner">
								<h3> Leave Review </h3>
								@if(Auth::guest())
									<textarea class="span12 disabled" disabled placeholder="You have to Login to write reviews"></textarea>
								@else
									<form action="" method="POST">
										{{csrf_field()}}
										<textarea class="span12" placeholder="Write your review to this product" name="review_content"></textarea>
										<button class="view_posts pull-right" href="#"> Submit Review </button>
									</form>
								@endif
							</div>
						</li>
					</ul>
				</div> -->
				
			</figure>
		</section>
	</section>

</section>
<!-- Page Content Container -->
@endsection

@push('scripts')
<script>
	
</script>
@endpush