@extends('layouts.master')

@section('title', 'Store')

@section('content')

<!-- Page Content Container -->
<section id="content" class="product_grid">
	<section class="container-fluid container">
		<section class="row-fluid">
			<section class="span10 product_view" id="product_grid">  
			@foreach($products->chunk(3) as $productRow)
				@foreach($productRow as $product)
					<figure class="span4 @if($loop->first) first @endif" id="product"> 
						<div class="product_img">
							<a href="{{route('products.show', $product->id)}}"><img src="{{asset($product->image_url)}}" alt="{{$product->name}}" /></a>
						</div>
						<div class="first product_description">
							<h3> <a href="{{route('products.show', $product->id)}}"> {{$product->name}}</a> </h3>
							<p>Panti Asuhan: {{$product->orphanage->name}}</p>
							<span class="price"> Rp{{$product->price}} </span>
							<a href="{{route('products.show', $product->id)}}" class="btn pull-right"> <i class="icon-shopping-cart"></i> </a>
						</div>
					</figure>
				@endforeach
				<hr />
			@endforeach
			</section>
		</section>
		<section class="row-fluid">
			{{$products->links('vendor.pagination.custom')}}
		</section>
		
	</section>
</section>

<!-- Page Content Container -->
@endsection
